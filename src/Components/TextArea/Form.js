import React, { useContext, useState,useEffect } from 'react';
import './Form.css';
import {ListProvider} from '../ListProvider/ListProvider';
import {SettingsProvider} from '../Settings/SettingsProvider';
import Title from "../Title/Title";





const Form = () => {
// Alert Component
  const Alert=()=>{
    return(
    <div className={"alert"}>
        <h5>Your task has been added to the tasklist</h5>
    </div>
    )
}

  //States
const [Input,setInputs] = useState("");
const [placeholder,setPlaceholder] = useState("Type in your task");
const [alertchecker,setAlertChecker] = useState(false);
const [items, setItems] = useContext(ListProvider);
const [darkTheme,setDarkTheme] = useContext(SettingsProvider);







const setLocalStorage=()=>{
    localStorage.setItem('items',JSON.stringify(items))
  }

  const getLocalStorage=()=>{
    if(localStorage.getItem('items')===null){
      localStorage.setItem('items',JSON.stringify([]))
    }
    else{
      let localTodo = JSON.parse(localStorage.getItem('items'));
      setItems(localTodo)
    }
  }

  useEffect(()=>{
    getLocalStorage()
    },[]); 

useEffect(()=>{
setLocalStorage()
})



const setdarkTheme=()=>{
  localStorage.setItem('dark',JSON.stringify(darkTheme))
}

const getdarkTheme=()=>{
  if(localStorage.getItem('dark')===null){
    localStorage.setItem('dark',JSON.stringify([]))
  }
  else{
    let localTodo = JSON.parse(localStorage.getItem('dark'));
    setDarkTheme(localTodo)
  }
}

useEffect(()=>{
 getdarkTheme()
  },[]); 

useEffect(()=>{
setdarkTheme()
})
















   const HandleChange=(e)=>{
        console.log(e.target.value)
        const NewInput =e.target.value;
        setInputs(NewInput)
   }// handles the input change


 

  const SubmitHandler=()=>{
    if(Input === ""){
            setPlaceholder("Type in task please");
            setAlertChecker(false)
        }
   else{
    setItems([ 
      ...items,{text:Input,completed:false,id:Math.random()*1000}
    ]);
    setInputs("");
    setAlertChecker(!false)
    
   }
  }
 // adds the tasks
   
    return ( 
    <div className="form-container">
    <div className="container">
        <div className="Title">
         <Title  Message={"Create Your Task"} />
          
          </div>
                    <form>
                    <div className="inputcontainer">
                    <input className="form-input" onChange={HandleChange}onFocus={()=>
                    setAlertChecker(false)} 
                    placeholder={placeholder} 
                    name="text" 
                    value={Input}
                    >
                    </input>
                    </div>

                    <button className="Submit-button primary" onClick={(e)=>{
                      e.preventDefault();
                      SubmitHandler()
                    }}> 
                      <span>Submit</span></button>
                    </form>

                 
               {alertchecker && 
               <Alert />
               
               }
                    </div>
    </div>
     );
}
 


export default Form;




