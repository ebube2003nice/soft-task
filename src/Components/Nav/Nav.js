import React from 'react';
import './Nav.css';
import {Link} from 'react-router-dom';

const Nav = () => {

    // function handling menu toggle
    const toggle=()=>{
        const navLinks = document.querySelector(".nav-links")
        const burgerMenu = document.querySelector(".menu-bar")
        navLinks.classList.toggle("nav-active")
        burgerMenu.classList.toggle("toggle")
    }
    
    return ( 
        <div>
            <nav className="primary">
             <div className="logo">
           <h3>Soft-Task</h3>
       </div> 
         <div  className="nav-links" >
        <Link 
        onClick={toggle}
        
        to="/">
        <p>Create Task</p>
       
        </Link>

        <Link 
        onClick={toggle}
        
        to="/test">
        <p>Tasks</p>
      
        </Link>
        <Link 
        
        onClick={toggle}
        
        to="/settings">
        <p>Settings</p>
        </Link>
        </div>

        <div onClick={toggle} className="menu-bar ">
            <span className="line1"></span>
            <span className="line2"></span>
            <span className="line3"></span>
        </div>
            </nav>
     
            </div>

     );
}
 
export default Nav;