import React,{useContext} from 'react';
import './App.css';
import {SettingsProvider} from './Components/Settings/SettingsProvider';
import Main from './Components/Main/Main';
import Nav from './Components/Nav/Nav';
import {ListItems} from './Components/ListProvider/ListProvider';
import {Status} from './Components/statusProvider/statusprovider';
import {FilteredTodos} from './Components/FilteredProvider/FilteredProvider';
import Footer from './Components/Footer/Footer';

function App() {
  const[darkTheme,setDarkTheme]= useContext(SettingsProvider);
  

      
  return (
    <div  id={darkTheme ? 'dark-theme':'light-theme'}  className="App">
    <ListItems>
  <Status>
  <FilteredTodos>
  
      <Nav/>  {/* Navigation */}
    
      <Main/>     {/* Main Component */}
   
      </FilteredTodos>
      </Status>

   </ListItems>

    </div>

  );
 
}

export default App;
