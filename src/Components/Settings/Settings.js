import React,{useContext,useEffect} from 'react';
import {SettingsProvider} from './SettingsProvider';
import './Settings.css';
import Title from "../Title/Title";

const Settings = () => {
      const [darkTheme,setDarkTheme] = useContext( SettingsProvider );
      const setdarkTheme=()=>{
        localStorage.setItem('dark',JSON.stringify(darkTheme))
      }
    
      const getdarkTheme=()=>{
        if(localStorage.getItem('dark')===null){
          localStorage.setItem('dark',JSON.stringify([]))
        }
        else{
          let localTodo = JSON.parse(localStorage.getItem('dark'));
          setDarkTheme(localTodo)
        }
      }
    
      useEffect(()=>{
       getdarkTheme()
        },[]); 
    
    useEffect(()=>{
    setdarkTheme()
    })
    
    
    return (
      <div>
      <Title Message={"Settings"} / >
        <div className="Settings">
          
            <button className="Toggle  primary" onClick={()=>{
    setDarkTheme(prevTheme=>!prevTheme)
}} 
           
            
    >Toggle Mode</button>




        </div>
        </div>
      );
}
 
export default Settings; 