import React,{useState,createContext} from 'react';
export const SettingsProvider = createContext();

export const SettingsState=props=>{
const [darkTheme,setDarkTheme] = useState(false)


return(
   <SettingsProvider.Provider value={[darkTheme,setDarkTheme]}>
      {props.children}
   </SettingsProvider.Provider>
);
}




