import React,{useState,createContext} from 'react';
export const StatusProvider = createContext();

export const Status=props=>{

const [status,setStatus] = useState("all")



return(
   <StatusProvider.Provider value={[status,setStatus] }>
      {props.children}
   </StatusProvider.Provider>
);
}
