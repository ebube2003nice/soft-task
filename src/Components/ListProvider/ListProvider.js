import React,{useState,createContext} from 'react';
export const ListProvider = createContext();

export const ListItems=props=>{
const [items,setItems] = useState([]);

return(
   <ListProvider.Provider value={[items,setItems] }>
      {props.children}
   </ListProvider.Provider>
);
}

