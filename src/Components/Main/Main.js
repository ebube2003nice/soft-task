import React from 'react';
import Form from '../TextArea/Form';
import Settings from '../Settings/Settings'
import{Route,Switch} from 'react-router-dom';
import List from '../List/List';
const Main = ({props}) => {

    return ( 
        <Switch>
     <Route exact path ="/" component={Form}></Route>
     <Route exact path ="/test" component={List}></Route>
     <Route exact path ="/settings" component={Settings}></Route>

        </Switch>

     );
}
 
export default Main;