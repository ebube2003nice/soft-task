import React,{useContext,useEffect, useState} from 'react';
import {ListProvider} from '../ListProvider/ListProvider';
import {SettingsProvider} from '../Settings/SettingsProvider'
import { StatusProvider } from '../statusProvider/statusprovider';
import {FilterProvider} from '../FilteredProvider/FilteredProvider';
import Todo from './Todo'
import "./List.css";
import Title from "../Title/Title";

const List = () => {
    const [items, setItems] = useContext(ListProvider);
    const [status,setStatus] = useContext(StatusProvider);
    const[darkTheme,setDarkTheme]= useContext(SettingsProvider);
    const [filteredTodos,setFilteredTodos] = useContext(FilterProvider);
    //FUNCTIONS

    const statusHandler=(e)=>{
      setStatus(e.target.value)
    }



const filteredHandler=()=>{
  switch(status){
    case'completed':
    setFilteredTodos(items.filter(item=> item.completed=== true));
    break;

    case 'uncompleted':
      setFilteredTodos(items.filter(item=> item.completed=== false));
    break;
    default:
    setFilteredTodos(items);
    break;
  }
}

useEffect(
  ()=>{
    filteredHandler()
  },
  [items,status]
)
    


      
     

     

    const setLocalTodos=()=>{
      localStorage.setItem('items',JSON.stringify(items))
    }

    const getLocalTodos=()=>{
      if(localStorage.getItem('items')=== null){
        localStorage.setItem('items',JSON.stringify([]))
      }

      else{
        let localTodos = JSON.parse(localStorage.getItem('items'));
        setItems(localTodos)
      }
    }

    useEffect(()=>{
      getLocalTodos()
    },[])

    useEffect(()=>{
      setLocalTodos()
    })








    const setdarkTheme=()=>{
      localStorage.setItem('dark',JSON.stringify(darkTheme))
    }
  
    const getdarkTheme=()=>{
      if(localStorage.getItem('dark')===null){
        localStorage.setItem('dark',JSON.stringify([]))
      }
      else{
        let localTodo = JSON.parse(localStorage.getItem('dark'));
        setDarkTheme(localTodo)
      }
    }
  
    useEffect(()=>{
     getdarkTheme()
      },[]); 
  
  useEffect(()=>{
  setdarkTheme()
  })
  
  
    
    return ( 
    <div  className="listcontainer">
      
       <Title Message={"Your Task List"}/>
       
        <div className="select">
        <select onChange={statusHandler}  name="todos" >
          <option value="all">all</option>
          <option value="completed">completed</option>
          <option value="uncompleted">uncompleted</option>
        </select>
        </div>

          <ul id={darkTheme ? 'dark-theme':'light-theme'}>
        {filteredTodos.map(item=>(
            <Todo text={item.text} 
            id={item.id} 
            key={item.id} 
            setItems={setItems} 
            todos={items}
            item={item}     
            />
          ))}

        </ul>       
            </div>
     );

}

 
export default List;