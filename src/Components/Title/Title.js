import React,{useContext} from 'react';

import  {SettingsProvider} from "../Settings/SettingsProvider";
const Title = ({Message}) => {

    const [darkTheme,setDarkTheme] = useContext(SettingsProvider);
    return ( 

<h1 id={darkTheme ? 'dark-theme':'light-theme'}>{Message}</h1>

     );
}
 
export default Title;