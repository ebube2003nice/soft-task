import  React from 'react';


const Todos = ({text,todos,setItems,item}) => {
    const deletehandler=()=>{
setItems(todos.filter(el=>el.id !== item.id))
    }

    const completeHandler=()=>{
     setItems(todos.map((list)=>{
         if(list.id===item.id){
             return{
                 ...list,completed: !list.completed
             }
         }
         return list
     }))
    }
    return ( 
        <div className="content primary">
            <li className={`todo-item  ${item.completed ? "completed" :"" } `} >{text}</li>
            <button onClick={deletehandler} className="delete"><i className="fa fa-trash"></i></button>
            <button onClick={completeHandler} className="complete"><i className="fa fa-check"></i></button>
          
        </div>

     );
}
<i className="far fa-trash-alt"></i>
export default Todos;