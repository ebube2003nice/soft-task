import React,{useState,createContext} from 'react';
export const FilterProvider = createContext();

export const FilteredTodos=props=>{

const [filteredTodos,setFilteredTodos] = useState([])


return(
   <FilterProvider.Provider value={[filteredTodos,setFilteredTodos] }>
      {props.children}
   </FilterProvider.Provider>
);
}

